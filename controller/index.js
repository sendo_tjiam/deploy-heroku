const StudentController = require("./student.controller")
const VehicleController = require("./vehicles.controller")
const ClassController = require("./class.controller")
const StudentClassController = require("./StudentClass.contoller")


module.exports = {
  StudentController,
  VehicleController,
  ClassController,
  StudentClassController
}