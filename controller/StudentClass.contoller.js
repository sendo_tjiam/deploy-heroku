const { Students, Class, StudentClass } = require("../models")

class StudentCLassController {
  static async getStudentClasses(req, res) {
    try {
      const options = {
        include: [
          {
            model: Students,
            attributes: ["name"]
          },
          {
            model: Class,
            attributes: ["name"]
          }
        ]
      }
      const data = await StudentClass.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }

  }

  static async getStudentClass(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      },
    }
    const data = await StudentClass.findOne(options)

    res.status(200).json({ data })
  }

  static async updateStudentCLass(req, res) {
    const { ClassId, StudentId } = req.body

    const payload = {
      ClassId, StudentId
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updated = await StudentClass.update(payload, options)

    res.status(200).json({
      data: updated
    })
  }

  static async createStudentClass(req, res) {
    try {
      console.log("====== create student class")
      const { ClassId, StudentId } = req.body
      const payload = {
        ClassId, StudentId
      }
      console.log("==== ini payload ==")
      console.log(payload)

      const newStudentClass = await StudentClass.create(payload)
      res.status(200).json({ data: newStudentClass })
    } catch (error) {
      console.log(error)
    }

  }


  static async deleteStudentClass(req, res) {
    const id = req.params.id
    const deleted = await StudentClass.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deleted
    })
  }
}

module.exports = StudentCLassController