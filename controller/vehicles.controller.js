const { Vehicles, Students } = require("../models")
const uuid = require("uuid")

class VehicleController {
  static async getVehicles(req, res) {
    try {
      const options = {
        include: [
          {
            model: Students,
            attributes: ["name"]
          }
        ]
      }
      const data = await Vehicles.findAll(options)
      res.status(200).json({ data })
    } catch (error) {
      console.log(error)
    }

  }

  static async getVehicle(req, res) {
    const id = req.params.id
    const options = {
      where: {
        id
      },
    }
    const data = await Vehicles.findOne(options)

    res.status(200).json({ data })
  }

  static async updateVehicle(req, res) {
    const { type, StudentId } = req.body

    const payload = {
      type, StudentId
    }
    const id = req.params.id
    const options = {
      where: {
        id
      },
      returning: true
    }
    const updated = await Vehicles.update(payload, options)

    res.status(200).json({
      data: updated
    })
  }

  static async createVehicle(req, res) {
    const id = uuid.v4()
    const { type, StudentId } = req.body
    const payload = {
      type, StudentId, id
    }

    const newVehicles = await Vehicles.create(payload)
    res.status(200).json({ data: newVehicles })
  }


  static async deleteVehicle(req, res) {
    const id = req.params.id
    const deleted = await Vehicles.destroy({
      where: {
        id
      },
      returning: true
    })

    res.status(200).json({
      data: deleted
    })
  }
}

module.exports = VehicleController