const { VehicleController } = require("../controller")
const route = require("express").Router()


route.get("/", VehicleController.getVehicles)
route.get("/:id", VehicleController.getVehicle)
route.post("/", VehicleController.createVehicle)
route.put("/:id", VehicleController.updateVehicle)
route.delete("/:id", VehicleController.deleteVehicle)


module.exports = route