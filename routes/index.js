const studentRoute = require("./student")
const vehicleRoute = require("./vehicles")
const classRoute = require("./class")
const studentClassRoute = require("./studentClass")
const route = require("express").Router()


route.use("/student", studentRoute)
route.use("/vehicle", vehicleRoute)
route.use("/class", classRoute)
route.use("/studentclass", studentClassRoute)


module.exports = route