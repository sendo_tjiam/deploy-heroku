'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Vehicles extends Model {
    static associate(models) {
      Vehicles.belongsTo(models.Students, { foreignKey: "StudentId" })
    }
  };
  Vehicles.init({
    id: {
      primaryKey: true,
      type: DataTypes.STRING
    },
    type: DataTypes.STRING,
    StudentId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Vehicles',
  });
  return Vehicles;
};