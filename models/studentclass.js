'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class StudentClass extends Model {

    static associate(models) {
      // StudentClass.belongsTo(models.Students)
      // StudentClass.belongsTo(models.Class)
      // models.Students.belongsToMany(models.Class, { through: models.StudentClass })
      // models.Class.belongsToMany(models.Students, { through: models.StudentClass })

    }
  };
  StudentClass.init({
    StudentId: DataTypes.STRING,
    ClassId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'StudentClass',
  });
  return StudentClass;
};